# Eksempel konfigurerings fil til backup af klienter med ALBackup.
# VERSION: 0.4.3
#

##############################################################################
# CLIENT: klientnavn | FromFile
#
# Der skal tages backup af denne klient.
#
# Dette kan v�re et navn p� en klient, IP adresse  eller det kan v�re
# v�rdien 'FromFile'. Hvis der bliver brugt 'FronFile' vil
# klientnavnet (hostname) blive taget ud fra filnavnet!. 
#
# F.eks.:
#   myClient.myDomain.com.conf bliver til myClient.myDomain.com
#
# Dette er en stor fordel hvis man har mange klienter der skal taget
# backup af p� den samme m�de. Det vil sige hvor kun klientnavnet er
# den eneste forskel. S� skal man kun lave �n konfigurerings fil for
# dem alle. Man kan s� lave en l�nke (link) til filen med kommandoen:
#
#	ln -s cli1.mydom.com.conf cli2.mydom.com.conf
#	eller
#	ln -s web_servers cli1.mydom.com.conf
#	ln -s web_servers cli2.mydom.com.conf
#
# F.eks.: 
#	  CLIENT="myclient.mydomain.com"
#	  CLIENT="myclient.mydomain.com.conf"
##############################################################################

CLIENT="server.mitdom�ne.dk"

##############################################################################
# CLIENT_IP: IP adresse
# 
# Denne indstilling giver mulighed for at bruge klientens IP adresse
# i stedet for klientnavn. IP adresse vil blive brugt under kommunikation
# over netv�rket, s� man bedre kan styre hvilken vej (route) der bruges
# mellem klient og server.
#
# Selve backuppen vil stadig blive gemt under klientnavnet !!!
#
##############################################################################

# Erstat 1.2.3.4 med IP fo server.mitdom�ne.dk
CLIENT_IP="root@1.2.3.4"

##############################################################################
# BASEDIR: sti
#
# Her vil alle backups blive gemt p� backup-serveren.
#
# F.eks.: BASEDIR="/home/backup"
##############################################################################

BASEDIR="/media/Backup"

##############################################################################
# BACKUP_DIRS: liste med stier.
#
# F�lgende mapper p� klienten skal der laves backup af.
#
# F.eks.: BACKUP_DIRS=(/etc /home /var /root)
##############################################################################

BACKUP_DIRS=(/home/ /etc /var/lib /opt)

##############################################################################
# USE_HARDLINK: Yes | No
#
# Skal inode linket (hardlink) filer kopieres som selvst�ndig filer
# eller som l�nke. 
#
# NOTAT:
#   Det skal bem�rkes at det tager betydelig l�ngere tid at tage
#   backup af hver klient ved brug af USE_HARDLINK !!!
#
# F.eks.: USE_HARDLINK="Yes"
##############################################################################

USE_HARDLINK="Yes"

##############################################################################
# EXCLUDES: [m�nster]
#
# Filer som ikke skal medtages i backuppen.
##############################################################################

EXCLUDES="*.iso"

##############################################################################
# EXCLUDE_FILE: Sti til filen.
#
# M�nstret er det samme som EXCLUDES. Men bruger en fil i stedet.
# 
# Filens struktur er et EXCLUDE m�nster pr. linie. Linier som begynder med
# '#' eller ';' bruges ikke.
#
# Evt. Se 'man rsync' og s�g efter 'EXCLUDE'
#
# Denne fil kunne f.eks. indeholde:
# ISO/
# TOOLS/BIN/
##############################################################################

EXCLUDE_FILE=""

##############################################################################
# BACKUP_EVERY: Day | EvenDay | OddDay | Week | EvenWeek | OddWeek |
#		Month | EvenMonth | OddMonth | Year
#
# Hvorn�r skal der tages backup af klienten.
# 
# Mulighederne er:
#
#   Day		En gang om dagen.
#   EvenDay	Hver anden dag p� lige datoer. Dag: 2, 4, 6, ...
#   OddDay	Hver anden dag p� ulige datoer. Dag: 1, 3, 5, ...
#   Week	En gang om ugen ved f�rste forekomst af BACKUP_DAY.
#   EvenWeek	Hver anden uge i lige uger. Uge: 2, 4, 6, ...
#   OddWeek	Hver anden uge i ulige uger. Uge: 1, 3, 5, ...
#   Month	En gang om  m�neden ved f�rste forekomst af BACKUP_DAY.
#   EvenMonth	Hver anden m�ned i ulige m�neder. M�ned: 2, 4, ...
#   OddMonth	Hver anden m�ned i lige m�neder. M�ned: 1, 3, ...
#   Year	En gang om �ret ved f�rste forekomst af BACKUP_DAY. 
#
# F.eks.: BACKUP_EVERY="OddWeek"
##############################################################################

BACKUP_EVERY="Day"

##############################################################################
# BACKUP_DAY: 0 | 1 | 2 | 3 | 4 | 5 | 6
#
# Hvilken dag i ugen skal der tages backup.
#
# Denne parameter har kun betydning n�r BACKUP_EVERY ikke er sat
# til BACKUP_EVERY="Day"
#
# Mulighederne er:
#
#  0   == S�ndag
#  1   == Mandag
#  2   == Tirsdag
#  3   == Onsdag
#  4   == Torsdag
#  5   == Fredag
#  6   == L�rdag
#
#  Eksempel: BACKUP_DAY="5"
##############################################################################

BACKUP_DAY="1"

##############################################################################
# EXEC_PRE: [sti + kommando]
#
# Handling der skal udf�res umiddelbart f�r selve backuppen udf�res.
# 
# F.eks. EXEC_PRE="/my/pre/backup-script/on/my/client.pl"
# 
##############################################################################

EXEC_PRE=""

##############################################################################
# EXEC_POST: [sti + kommando]
#
# Handling der skal udf�res umiddelbart efter selve backuppen er
# udf�rt.
#
# F.eks. EXEC_POST="/my/post/backup-script/on/my/client.sh"
##############################################################################

EXEC_POST=""

##############################################################################
# EXEC_ERROR_CONTINUE: Yes | No
#
# Skal der fors�ttes med at tabe backup af klienten hvis der er
# opst�et en fejl ved k�rsel af EXEC_PRE
#
##############################################################################

EXEC_ERROR_CONTINUE="No"
